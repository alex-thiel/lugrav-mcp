![Bitbucket open issues](https://img.shields.io/bitbucket/issues-raw/alex-thiel/lugrav-mcp)

# TRON, das Master Control Program der lugrav #

TRON ist ein discord bot der im Server der lugrav verfügbar ist. Wir wollen diesen Bot weiter
ausbauen und ihm weitere sinnvolle Features hinzufügen.

Wenn ihr Anforderungen oder gute Ideen habt, meldet die bitte im lugrav discord Server.

# Run containers with docker-compose

* build docker images as precondition to use docker-compose

* setup `.env` file for local environment or supply the required parameters in the compose file.
```
DISCORD_ID=549686778382954060
DISCORD_TOKEN=ylLjdMCNOFRHswbycyxjKJXL.sJFAgM.JUuDJAhzHjsdjdnPEQSVdktJMHG
```

* start containers
```SH
docker-compose up -d
```

* stop/remove containers
```SH
docker-compose down
```


# Lizenz #

TRON (lugrav-mcp) ist Freie Software: Sie können es unter den Bedingungen der GNU General Public License,
wie von der Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren veröffentlichten
Version, weiter verteilen und/oder modifizieren.

TRON (lugrav-mcp) wird in der Hoffnung, dass es nützlich sein wird, aber OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt;
sogar ohne die implizite Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK. Siehe die
GNU General Public License für weitere Details.

Mit dem Programm erhalten sie eine Kopie der GNU General Public License.
Wenn nicht, siehe <https://www.gnu.org/licenses/> für die komplette Lizenzdatei.

Copyright (c) 2021 ALX-Development

<http://https://www.alx-development.de/>
