FROM python:3.8
MAINTAINER ALX-Development (alex@alx-development.de)

# Setting up the default working directory
COPY ./app /app
WORKDIR /app

# Installing the python requirements
COPY ./requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

# Launching the main application
CMD ["python", "main.py"]
