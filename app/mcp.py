#!/usr/bin/python3
import logging
import os
from random import randrange

import discord


class Tron(discord.Client):
    id = None
    logger = None
    cites = []

    # Logging in to the server
    @staticmethod
    async def on_ready():
        Tron.logger = logging.getLogger("tron")
        Tron.id = os.getenv('DISCORD_ID')
        Tron.logger.debug("I've been logged in with id [" + Tron.id + "]")

        # Loading the cites as array
        with open('cite.txt') as f:
            Tron.cites = [line.rstrip() for line in f]

    @staticmethod
    async def on_message(message):
        # A message has been received
        Tron.logger.debug("Message from " + str(message.author.name) + ": [" + message.content + "]")

        # Checking if I've been mentioned inside the message
        mentioned = False
        Tron.logger.debug("Mentions: " + str(message.mentions))
        for user in message.mentions:
            if str(user.id) == Tron.id:
                mentioned = True

        # An answer is prepared either if I#ve been mentioned or th prefix "!mcp" has been called
        if mentioned or message.content.startswith("!mcp "):
            Tron.logger.debug("I've been mentioned in the message, preparing a response")

            random_cite = randrange(len(Tron.cites))
            await message.channel.send('Hallo '
                                       + str(message.author.name)
                                       + ', wusstest du schon? '
                                       + Tron.cites[random_cite])
