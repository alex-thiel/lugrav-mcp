#!/usr/bin/python3
import os
import logging

from dotenv import load_dotenv
from mcp import Tron

if __name__ == '__main__':
    # ------------------------------------------------------------------
    # Importing environment variables
    load_dotenv()

    # ------------------------------------------------------------------
    # Logging Setup
    logging.basicConfig(level=logging.DEBUG,
                        # filename=os.getenv('LOGFILE', './mcp.log'),
                        # filemode='w',
                        format='%(asctime)s %(levelname)-8s [%(name)-20s] %(message)s'
                        )

    # This is to get the logging information during the development process more detailed
    logging.getLogger("tron").setLevel(logging.DEBUG)
    logging.info('Environment and logging has been initialized')

    TOKEN = os.getenv('DISCORD_TOKEN')
    if TOKEN:
        logging.debug("Token: " + TOKEN)
        discord_bot = Tron()
        discord_bot.run(TOKEN)
    else:
        logging.error("No discord bot token supplied, exiting")
