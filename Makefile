#Including the environmental setup
include .env

#vars
IMAGE_DOMAIN="alx"
IMAGE_NAME="mcp"
#TERMINAL=eval
# Linux with GUI
TERMINAL=xterm -bg black -fg white -geometry 120x40 -title "DOCKER: ${IMAGE_NAME}" -e
# Windows with GUI
#TERMINAL=wt -w 0 powershell

.PHONY: help build run stop connect deploy all
.DEFAULT_GOAL := all

help:
		@echo "Makefile arguments:"
		@echo " "
		@echo "Makefile commands:"
		@echo "build   - Building a container image"
		@echo "run     - Starting a pre-build container"
		@echo "stop    - Shutting down a running container"
		@echo "connect - Opens a connection to the container intern bash"
		@echo "all     - Doing both in one step: building and starting the container"
		@echo "deploy  - Building the tarball for deployment to portainer or similar"

build: Dockerfile
		@echo Building image ${IMAGE_DOMAIN}/${IMAGE_NAME} from $<
		@docker build -t ${IMAGE_DOMAIN}/${IMAGE_NAME} .

run: docker-compose.yml
		@echo "Starting the docker container in separate terminal"
		@${TERMINAL} "docker-compose --file $< up" &
		@echo "To stop the container execution, run 'make stop'"

stop: docker-compose.yml
		@docker-compose --file $< down

connect:
		@docker exec -it ${IMAGE_NAME} /bin/bash

deploy: dist/mcp.tar

dist/mcp.tar: Dockerfile requirements.txt app/main.py app/mcp.py app/cite.txt
		@mkdir --parents dist/
		@echo "Creating portainer tarball"
		@tar --create --file $@ $^

all: build run